August 17, 2020

# Release announcement: Bitcoin Cash Node v22.0.0

The Bitcoin Cash Node (BCHN) project is pleased to announce its major release
version 22.0.0.

This release implements the November 15, 2020 network upgrade.
It contains the ASERT difficulty algorithm, improved documentation
and a number of bugfixes and performance improvements.

For the full release notes, please visit:

  https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v22.0.0

Executables and source code for supported platforms are available at the
above link, or via the download page on our project website at

  https://bitcoincashnode.org

For more information about the November 15, 2020 network upgrade, visit

  https://upgradespecs.bitcoincashnode.org/2020-11-15-upgrade/

We hope you enjoy our latest release and invite you to join us to improve
Bitcoin Cash.

Sincerely,

The Bitcoin Cash Node team.
