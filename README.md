# Bitcoin Cash Node announcements

This repository contains a list of public announcements.

Please refer to the contained files which should be named in the format

    YYYYmmdd_<some description with underscores instead of whitepace>.md

## Want to sign an announcement to express your approval?

You can use the git hash of the commit of the version you approve, as part of your signing 'message'.
This strongly identifies the contents that you're signing.

You can alternatively download the raw text, and sign a digest of that.

Ways to sign(al) your support:

*  Bitcoin signing
*  PGP / GPG
*  keybase sign
*  [memo.cash](https://memo.cash) or [Member](https://memberapp.github.io/) posts

Please help us to improve by raising issues if you spot something wrong.

Thanks,

\- The Bitcoin Cash Node team